import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

import 'package:numa/conf.dart';
import 'package:numa/main.dart';
import 'package:numa/king/king.dart';

import 'mock/king.dart';

void main() {
  testWidgets('TestName: Show roll', (WidgetTester tester) async {
    await tester.pumpWidget(NumaApp(king: mockKing()));
    await tester.pump();

    //expect(find.text('Sign in as 2'), findsOneWidget);
    //await tester.tap(find.text('Sign in as 2'));
    //await tester.pump();

    //expect(find.text('My Profile'), findsOneWidget);
    //await tester.tap(find.text('My Profile'));
    //await tester.pump();
    //// have to wait a second time as profile is loaded asynchronously and has a wait
    //await tester.pump();

    //expect(find.text('JG'), findsOneWidget);
    //expect(find.text('No description'), findsOneWidget);
    //expect(find.text('Jim Testing Guy'), findsOneWidget);

    //debugDumpApp();
    printSuccess();
  });
}
