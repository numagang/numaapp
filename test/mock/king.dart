import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

import 'package:numa/conf.dart';
import 'package:numa/king/king.dart';

King mockKing() {
  Conf conf = Conf(whichApi: ApiChoices.mock);
  King king = King(conf: conf);
  return king;
}

printSuccess([String name = '']) {
  print('.-.-.-.-.-. $name TEST SUCCESSFUL  .-.-.-.-.');
}

//printText(WidgetTester tester) {
//var finder = find.text(search);
printText(Finder finder) {
  var text = finder.evaluate().single.widget as TextFormField;
  print(text.toString());
}
