import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';

import 'package:numa/king/king.dart';
import 'package:numa/widgets/account/account.dart';
//import 'package:numa/widgets/auth/router.dart';
import 'package:numa/widgets/based/scaffold.dart';
import 'package:numa/widgets/roll/roll.dart';
import 'package:numa/widgets/convo/convo_list.dart';

class NavStack extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var king = fromKing(context, Viceroys.king);
    return Observer(
      builder: (_) => IndexedStack(
        key: king.scaffoldKey,
        index: king.navman.currentIndex.index,
        children: <Widget>[
          Navigator(
            key: king.navman.accountNavKey,
            onGenerateRoute: (RouteSettings route) => MaterialPageRoute(
              settings: route,
              builder: (context) => BasedScaffold(child: AccountPage()),
            ),
          ),
          Navigator(
            key: king.navman.rollNavKey,
            onGenerateRoute: (RouteSettings route) => MaterialPageRoute(
              settings: route,
              builder: (context) => BasedScaffold(child: RollPage()),
            ),
          ),
          Navigator(
            key: king.navman.convoNavKey,
            onGenerateRoute: (RouteSettings route) => MaterialPageRoute(
              settings: route,
              builder: (context) => BasedScaffold(child: ConvoListPage()),
            ),
          ),
          Navigator(
            key: king.navman.authNavKey,
            onGenerateRoute: (RouteSettings route) => MaterialPageRoute(
              settings: route,
              builder: (context) => BasedScaffold(child: MapPage()),
            ),
          ),
        ],
      ),
    );
  }
}

class MapPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var king = fromKing(context, Viceroys.king);
    return Text('map page holder on page ${king.navman.currentIndex}');
  }
}
