// TODO: create OfflineMockLip for customer use
enum ApiChoices { napi, mock }

class Conf {
  final String apiUrl = 'http://192.168.1.229:9090/v1/';
  final String env = 'dev';
  final String logLevel = 'all';

  final String tzFormat = 'standard';

  final double defaultLat = 40.7228;
  final double defaultLng = -74.0000;
  final double defaultLatDelta = 0.0230;
  final double defaultLngDelta = 0.0105;

  final int requestTimeout = 5;

  final int cacheTimeoutMinutes = 1;

  ApiChoices whichApi;

  Conf({ApiChoices whichApi}) : this.whichApi = whichApi {
    print('Initiating configuration:');
    print(apiUrl);
    this.printApiChoice();
  }

  void printApiChoice() {
    String name = 'notset';
    switch (this.whichApi) {
      case ApiChoices.napi:
        name = 'napi';
        break;
      case ApiChoices.mock:
        name = 'mock';
        break;
      default:
        this.printApiChoice();
        throw ('Invalid api set by conf.whichApi');
        break;
    }
    print('Selected API: ' + name);
  }

  void selectApi(ApiChoices api) {
    this.whichApi = api;
  }
}
