import 'dart:core';
import 'package:bubble/bubble.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:mobx/mobx.dart';

import 'package:numa/dad/convo_cache.dart';
import 'package:numa/dad/dad.dart';
import 'package:numa/widgets/based/images.dart';

class ConvoPage extends StatelessWidget {
  String convoUuid;
  ConvoPage({this.convoUuid});

  @override
  Widget build(BuildContext context) {
    var messageList =
        getItemFromDad(context, CacheTypes.convo, '1111').messageList;

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.green,
        title: Row(
          children: <Widget>[
            Text('IM'),
            SizedBox(width: 16),
            Text('title goes'),
          ],
        ),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.more_vert, color: Colors.white),
            onPressed: () {},
          ),
        ],
      ),
      body: SizedBox.expand(
        child: Container(
          color: Colors.grey[900],
          child: Padding(
            padding: EdgeInsets.fromLTRB(8, 0, 8, 0),
            child: ListView.separated(
                itemCount: messageList.length,
                separatorBuilder: (BuildContext context, int index) =>
                    SizedBox(height: 2),
                itemBuilder: (context, index) {
                  var message = messageList[index];

                  var isNextMessageSameUser = false;
                  if ((index + 1) < messageList.length) {
                    var nextMessage = messageList[index + 1];
                    isNextMessageSameUser =
                        message.userUuid == nextMessage.userUuid;
                  }

                  var isPrevMessageSameUser = false;
                  if ((index - 1) > 0) {
                    var prevMessage = messageList[index - 1];
                    isPrevMessageSameUser =
                        message.userUuid == prevMessage.userUuid;
                  }

                  return MessageBubble(message,
                      isNextMessageSameUser: isNextMessageSameUser,
                      isPrevMessageSameUser: isPrevMessageSameUser);
                }),
          ),
        ),
      ),
    );
  }
}

class MessageBubble extends StatelessWidget {
  Message message;
  bool isMe;
  bool isNextMessageSameUser;
  bool isPrevMessageSameUser;

  MessageBubble(Message message,
      {this.isNextMessageSameUser = false,
      this.isPrevMessageSameUser = false}) {
    this.message = message;
    if (this.message.userUuid == '1111') {
      this.isMe = true;
    } else {
      this.isMe = false;
    }
  }

  @override
  Widget build(BuildContext context) {
    // TODO: smaller margin for messages from same user
    return Row(
      crossAxisAlignment: CrossAxisAlignment.end,
      mainAxisAlignment:
          this.isMe ? MainAxisAlignment.end : MainAxisAlignment.start,
      children: <Widget>[
        (!this.isMe && !this.isNextMessageSameUser)
            ? BasedCircleImage(
                src: '',
                radius: 18,
                //altText: account.username.substring(0, 2)),
                altText: 'bb',
              )
            : SizedBox(height: 0, width: 36),
        Flexible(
          child: Bubble(
            margin: this.isNextMessageSameUser
                ? BubbleEdges.only(top: 0, bottom: 0)
                : BubbleEdges.only(top: 0, bottom: 4),
            nip: this.isMe ? BubbleNip.rightBottom : BubbleNip.leftBottom,
            color: this.isMe ? Colors.green[600] : Colors.grey[850],
            child: MessageBody(
                this.message, this.isMe, this.isPrevMessageSameUser),
            //child:
            //Text(this.message.text, style: TextStyle(color: Colors.white)),
          ),
        ),
      ],
    );
  }
}

// TODO: do we want to make this fancy message body thing?
class MessageBody extends StatelessWidget {
  Message message;
  bool isMe;
  bool isPrevMessageSameUser = false;
  MessageBody(this.message, this.isMe, this.isPrevMessageSameUser);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment:
          this.isMe ? CrossAxisAlignment.end : CrossAxisAlignment.start,
      children: <Widget>[
        (!this.isPrevMessageSameUser && !this.isMe)
            ? Text(this.message.username, style: TextStyle(color: Colors.blue))
            : SizedBox(height: 0),
        Row(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            SizedBox(width: 2),
            Flexible(
                child: Text(this.message.text,
                    style: TextStyle(color: Colors.white))),
            SizedBox(width: 2),
          ],
        ),
        SizedBox(height: 4),
        // TODO: do we want/need a timestamp?
        //IntrinsicWidth(
        //child: Row(
        //children: <Widget>[
        //Text(this.message.timeReadable(),
        //style: TextStyle(color: Colors.grey[300], fontSize: 10)),
        //],
        //),
        //),
      ],
    );
  }
}
