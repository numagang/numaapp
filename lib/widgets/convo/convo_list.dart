import 'dart:core';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:mobx/mobx.dart';

import 'package:numa/dad/dad.dart';
import 'package:numa/dad/convo_list_cache.dart';
import 'package:numa/widgets/convo/convo.dart';

class ConvoListPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.grey[900],
      child: ConvoListWidget(userUuid: '1111'),
    );
  }
}

class ConvoListWidget extends StatelessWidget {
  String userUuid;
  ConvoListWidget({this.userUuid});

  @override
  Widget build(BuildContext context) {
    var convoList =
        getItemFromDad(context, CacheTypes.convoList, '1111').convoList;

    return Observer(
      builder: (_) => ListView.separated(
          itemCount: convoList?.length ?? 0,
          separatorBuilder: (BuildContext context, int index) =>
              Divider(color: Colors.black, height: 0),
          itemBuilder: (context, index) {
            var convo = convoList[index];

            return Material(
              color: Colors.grey[850],
              child: InkWell(
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (BuildContext context) =>
                              ConvoPage(convoUuid: convo.convoUuid)));
                },
                child: Padding(
                  padding: EdgeInsets.fromLTRB(8, 16, 8, 16),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      FlutterLogo(),
                      SizedBox(width: 8),
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Text(convo.title,
                                    style: TextStyle(color: Colors.white)),
                                Text(
                                    'Expires: ' +
                                        convo.expirationTimeReadable(),
                                    style: TextStyle(
                                        color: Colors.grey, fontSize: 12)),
                              ],
                            ),
                            SizedBox(height: 4),
                            Row(
                              children: <Widget>[
                                Text(convo.lastMessageUsername + ": ",
                                    style: TextStyle(color: Colors.white)),
                                Expanded(
                                  child: Text(
                                    convo.lastMessage,
                                    overflow: TextOverflow.ellipsis,
                                    style: TextStyle(color: Colors.grey),
                                  ),
                                ),
                                Text(convo.atCount.toString(),
                                    style: TextStyle(
                                        color: Colors.grey, fontSize: 12)),
                                SizedBox(width: 8),
                                Text(convo.unreadCount.toString(),
                                    style: TextStyle(
                                        color: Colors.grey, fontSize: 12)),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            );
          }),
    );
  }
}
