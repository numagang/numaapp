import 'dart:io';
import 'dart:async';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:international_phone_input/international_phone_input.dart';

import 'package:numa/king/king.dart';
import 'package:numa/stores/user.dart';
import 'package:numa/widgets/based/forms.dart';

class RegisterFormFields {
  String email;
  // TODO: detect region to set initial isoPhone
  String isoPhone = '+1';
  String nakedPhone = '';
  String isoCode = '+1';
  String password;
}

class RegisterPage extends StatefulWidget {
  @override
  RegisterPageState createState() {
    return RegisterPageState();
  }
}

class RegisterPageState extends State<RegisterPage> {
  final _formKey = GlobalKey<FormState>();
  bool _isLoading = false;
  RegisterFormFields fields = RegisterFormFields();

  void onPhoneNumberChange(String number, String intlNumber, String isoCode) {
    fields.nakedPhone = number;
    fields.isoCode = isoCode;
    fields.isoPhone = isoCode + '+' + number;
    validateEmailAndPhone(fields.email);
  }

  String validateEmailAndPhone(String email) {
    var phone = fields.nakedPhone;
    if (phone != '') {
      if (phone.length < 4) {
        return 'Phone number is too short';
      }
    }
    if (email != '') {
      if (email.length < 5) {
        return 'Email is too short';
      }
      int atPosition = email.indexOf('@');
      int dotPosition = email.lastIndexOf('.');
      if (dotPosition == -1 || atPosition == -1 || dotPosition <= atPosition) {
        return 'Email format is invalid';
      }
    }
    if (email == '' && phone == '') {
      return 'You must provide an email, a phone number, or both';
    }
    return null;
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Padding(
        padding: const EdgeInsets.all(12.0),
        child: Column(
          children: <Widget>[
            SizedBox(height: 40),
            Text(
              'You can register with your email, your phone number, or both!',
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 24),
            ),
            SizedBox(height: 14),
            TextFormField(
              decoration: InputDecoration(
                labelText: 'Email',
              ),
              maxLength: 100,
              autovalidate: true,
              autocorrect: false,
              onChanged: (value) {
                fields.email = value;
              },
              validator: (value) {
                return validateEmailAndPhone(value);
              },
            ),
            SizedBox(height: 14),
            InternationalPhoneInput(
              onPhoneNumberChange: onPhoneNumberChange,
              initialPhoneNumber: fields.nakedPhone,
              initialSelection: fields.isoPhone,
            ),
            SizedBox(height: 14),
            BasedCreatePassword(
                onChanged: (password) => fields.password = password),
            SizedBox(height: 10),
            _buildButton(context),
          ],
        ),
      ),
    );
  }

  Widget _buildButton(BuildContext context) {
    var user = fromKing(context, Viceroys.user);
    if (_isLoading) {
      return Center(child: CircularProgressIndicator());
    } else {
      return RaisedButton(
        onPressed: () async {
          setState(() {
            _isLoading = true;
          });
          if (_formKey.currentState.validate()) {
            await user.registerAndAuth(
                email: fields.email,
                isoPhone: fields.isoPhone,
                nakedPhone: fields.nakedPhone,
                isoCode: fields.isoCode,
                password: fields.password);
          }
          setState(() {
            _isLoading = false;
          });
          if (user.signedIn()) {
            user.pushToHomeNav();
          } else {
            Scaffold.of(context)
                .showSnackBar(SnackBar(content: Text('Registration failed')));
          }
        },
        child: Text('Register'),
      );
    }
  }
}
