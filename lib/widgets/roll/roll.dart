import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';

import 'package:numa/dad/dad.dart';
import 'package:numa/dad/roll_manager_cache.dart';
import 'package:numa/widgets/roll/image.dart';

class RollPage extends StatelessWidget {
  RollManager rollManager;

  @override
  Widget build(BuildContext context) {
    // TODO: use userUuid
    var rollManagerCache = fromDad(context, CacheTypes.rollManager);
    this.rollManager = rollManagerCache.getItem('fakeUserUuid');
    this.rollManager.updateCurrentMeme();

    return Container(
      color: Colors.grey[850],
      child: Observer(
        builder: (_) => rollManager.noMoreMemes
            ? Text('no more memes')
            : rollManager.loadingMemeData
                ? Text('loading')
                : Stack(
                    children: <Widget>[
                      Container(
                        constraints: BoxConstraints.expand(),
                        margin: EdgeInsets.only(bottom: 80),
                        child: RollImage(
                            memeUuid: this.rollManager.currentMemeUuid),
                      ),
                      Container(
                        alignment: Alignment.bottomCenter,
                        padding: EdgeInsets.only(bottom: 4),
                        child: Row(
                          children: <Widget>[
                            Spacer(),
                            Expanded(
                              child: ReactionSelector(Reactions.angryRed),
                            ),
                            Spacer(),
                            Expanded(
                              child: ReactionSelector(Reactions.cry),
                            ),
                            Spacer(),
                            Expanded(
                              child: ReactionSelector(Reactions.heartEyes),
                            ),
                            Spacer(),
                            Expanded(
                              child: ReactionSelector(Reactions.laughTears),
                            ),
                            Spacer(),
                          ],
                        ),
                      ),
                    ],
                  ),
      ),
    );
  }
}

class ReactionSelector extends StatelessWidget {
  final Reactions reaction;
  RollManager rollManager;
  ReactionSelector(Reactions reaction)
      : this.reaction = reaction,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    // TODO: use userUuid
    var rollManagerCache = fromDad(context, CacheTypes.rollManager);

    return GestureDetector(
      onTap: () {
        //final snackBar = SnackBar(content: Text('$reaction'));
        //Scaffold.of(context).showSnackBar(snackBar);
        this.rollManager = rollManagerCache.getItem('fakeUserUuid');
        this.rollManager.submitReaction(this.reaction, 'fakeUserUuid');
      },
      child: ReactionImage(this.reaction),
    );
  }
}
