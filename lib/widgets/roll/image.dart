import 'package:flutter/material.dart';

import 'package:numa/dad/roll_manager_cache.dart';

class RollImage extends StatelessWidget {
  final String memeUuid;
  final String memePath;
  final String altText;
  RollImage({String memeUuid, String altText})
      : this.memeUuid = memeUuid,
        this.memePath = 'assets/testImages/' + memeUuid + '.jpg',
        this.altText = altText,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    // TODO: show image title
    // TODO: be able to load NetworkImage
    return Image(image: AssetImage(this.memePath));
  }
}

class ReactionImage extends StatelessWidget {
  final Reactions reaction;
  final String reactionPath;
  final String altText;
  ReactionImage(Reactions reaction)
      : this.reaction = reaction,
        this.reactionPath =
            'assets/reactions/' + reaction.toString().split('.').last + '.png',
        this.altText = '$reaction',
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return Image(image: AssetImage(reactionPath));
  }
}
