import 'package:flutter/material.dart';

import 'package:numa/dad/dad.dart';

class BasedFieldUpdater extends StatefulWidget {
  final String initialValue;
  final String labelText;
  final int maxLength;
  final bool autocorrect;
  final CacheTypes cacheType;

  BasedFieldUpdater({
    Key key,
    @required String initialValue,
    @required String labelText,
    @required int maxLength,
    @required bool autocorrect,
    @required CacheTypes cacheType,
  })  : this.initialValue = initialValue,
        this.labelText = labelText,
        this.maxLength = maxLength,
        this.autocorrect = autocorrect,
        this.cacheType = cacheType,
        super(key: key);

  @override
  _BasedFieldUpdaterState createState() {
    return _BasedFieldUpdaterState(
        initialValue: this.initialValue,
        labelText: this.labelText,
        maxLength: this.maxLength,
        autocorrect: this.autocorrect);
  }
}

class _BasedFieldUpdaterState extends State<BasedFieldUpdater> {
  String initialValue;
  String labelText;
  int maxLength;
  bool autocorrect;
  CacheTypes cacheType;
  bool editMode = false;

  _BasedFieldUpdaterState({
    @required this.initialValue,
    @required this.labelText,
    @required this.maxLength,
    @required this.autocorrect,
    @required this.cacheType,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        _buildText(),
        _buildButtons(),
      ],
    );
  }

  Widget _buildText() {
    if (editMode) {
      return TextFormField(
        decoration: InputDecoration(labelText: this.labelText),
        initialValue: this.initialValue,
        maxLength: this.maxLength,
        autovalidate: true,
        autocorrect: this.autocorrect,
        validator: (value) {
          //return validateEmailAndPhone(value);
        },
      );
    }
    return Text(this.initialValue, style: TextStyle(fontSize: 16, height: 3.0));
  }

  Widget _buildButtons() {
    if (editMode) {
      return Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          FlatButton(
            child: Icon(
              Icons.check,
              size: 30.0,
            ),
            onPressed: () {
              // TODO: save field
              this.setState(() {
                this.editMode = false;
              });
            },
          ),
          FlatButton(
            child: Icon(
              Icons.cancel,
              size: 30.0,
            ),
            onPressed: () {
              this.setState(() {
                this.editMode = false;
              });
            },
          ),
        ],
      );
    }

    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: <Widget>[
        SizedBox(),
        FlatButton(
          child: Icon(
            Icons.edit,
            size: 30.0,
          ),
          onPressed: () {
            this.setState(() {
              this.editMode = true;
            });
          },
        ),
      ],
    );
  }
}
