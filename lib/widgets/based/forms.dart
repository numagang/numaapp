import 'package:flutter/material.dart';

class BasedCreatePassword extends StatefulWidget {
  Function onSaved;
  Function onChanged;
  BasedCreatePassword({Key key, this.onSaved, this.onChanged})
      : super(key: key);

  @override
  _BasedCreatePasswordState createState() {
    return _BasedCreatePasswordState();
  }
}

class _BasedCreatePasswordState extends State<BasedCreatePassword> {
  String _password = "";
  bool _obscurePassword = true;

  void _toggleObscurePassword() {
    setState(() {
      _obscurePassword = !_obscurePassword;
    });
  }

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      obscureText: _obscurePassword,
      enableSuggestions: false,
      maxLength: 100,
      autovalidate: true,
      decoration: InputDecoration(
        labelText: 'Password',
        suffixIcon: Padding(
          padding: EdgeInsets.only(top: 15.0),
          child: _buildEye(),
        ),
      ),
      validator: (value) {
        if (value.length < 4) {
          return 'Password is too short';
        }
        return null;
      },
      onChanged: (password) => widget.onChanged(password),
    );
  }

  Widget _buildEye() {
    Color color = _obscurePassword ? Colors.grey : Colors.green;
    return IconButton(
        icon: Icon(Icons.remove_red_eye),
        color: color,
        onPressed: () {
          _toggleObscurePassword();
        });
  }
}
