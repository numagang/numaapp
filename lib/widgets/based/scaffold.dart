import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';

import 'package:numa/king/king.dart';

class BasedScaffold extends StatelessWidget {
  Widget child;
  BasedScaffold({this.child});

  @override
  Widget build(BuildContext context) {
    var king = fromKing(context, Viceroys.king);
    return Scaffold(
      // TODO: we might not want to set this at such a high level
      //resizeToAvoidBottomInset: true,
      body: this.child,
      appBar: _buildAppBar(context, king),
    );
  }

  Widget _buildAppBar(BuildContext context, King king) {
    var navman = king.navman;
    final selectedItemColor = Colors.yellow;
    final unselectedItemColor = Colors.grey[100];

    if (navman.currentIndex == NavIndices.auth) {
      return Container(width: 0, height: 0);
    } else {
      return AppBar(
        backgroundColor: Colors.green,
        actions: <Widget>[
          Expanded(child: SizedBox(width: 10)),
          IconButton(
            icon: Icon(Icons.account_circle,
                color: navman.isCurrentIndex(NavIndices.account)
                    ? selectedItemColor
                    : unselectedItemColor),
            onPressed: () {
              navman.switchToNav(NavIndices.account);
            },
          ),
          Expanded(child: SizedBox(width: 10)),
          IconButton(
            icon: Icon(Icons.camera_roll,
                color: navman.isCurrentIndex(NavIndices.roll)
                    ? selectedItemColor
                    : unselectedItemColor),
            onPressed: () {
              navman.switchToNav(NavIndices.roll);
            },
          ),
          Expanded(child: SizedBox(width: 10)),
          IconButton(
            icon: Icon(Icons.chat_bubble,
                color: navman.isCurrentIndex(NavIndices.convo)
                    ? selectedItemColor
                    : unselectedItemColor),
            onPressed: () {
              navman.switchToNav(NavIndices.convo);
            },
          ),
          Expanded(child: SizedBox(width: 10)),
        ],
      );
    }
  }
}
