import 'package:flutter/material.dart';

class BasedWrapperPadded extends StatelessWidget {
  Widget wrapped;
  BasedWrapperPadded(this.wrapped) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.green,
      child: SafeArea(
        child: Container(
          color: Colors.white,
          child: Stack(
            children: [
              Padding(
                padding: EdgeInsets.all(8.0),
                child: wrapped,
              ),
              Positioned(
                child: BackArrow(),
                top: 10,
                left: 10,
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class BasedWrapperNoScrollTopPadded extends StatelessWidget {
  Widget top;
  Widget body;
  int bodyFlex;
  BasedWrapperNoScrollTopPadded({this.top, this.body, this.bodyFlex})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.green,
      child: SafeArea(
        child: Container(
          color: Colors.white,
          child: Stack(
            children: [
              Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Expanded(
                    child: SingleChildScrollView(
                      child: Container(
                        padding: const EdgeInsets.all(8.0),
                        child: this.top,
                      ),
                    ),
                  ),
                  Expanded(
                    flex: this.bodyFlex,
                    child: this.body,
                  ),
                ],
              ),
              Positioned(
                child: BackArrow(),
                top: 10,
                left: 10,
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class BackArrow extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    if (Navigator.of(context).canPop()) {
      return Container(
        color: Color.fromRGBO(255, 255, 255, 0.7),
        child: GestureDetector(
          onTap: () {
            // Note: we check canPop() against, because if we navigate from
            // another NavKey that can go back, then the back arrow will appear
            // no matter what.
            if (Navigator.of(context).canPop()) {
              Navigator.of(context).pop();
            }
          },
          child: Icon(
            Icons.arrow_back,
            size: 30.0,
          ),
        ),
      );
    } else {
      return Container(width: 0.0, height: 0.0);
    }
  }
}
