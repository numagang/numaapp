import 'package:flutter/material.dart';

import 'package:numa/king/king.dart';

class BasedPageTitle extends StatelessWidget {
  final String title;
  BasedPageTitle(this.title) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Text(
        title,
        style: TextStyle(height: 1, fontSize: 26),
      ),
    );
  }
}

class BasedFormFieldTitle extends StatelessWidget {
  final String title;
  BasedFormFieldTitle(this.title) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.topLeft,
      child: Text(
        title,
        style: TextStyle(fontSize: 16),
      ),
    );
  }
}

class BasedRightOptionPusher extends StatelessWidget {
  final String text;
  final Color textColor;
  Widget pushTo;

  BasedRightOptionPusher({Key key, this.text, this.textColor, this.pushTo})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      child: Text(this.text, style: TextStyle(color: this.textColor)),
      onTap: () {
        Navigator.push(context,
            MaterialPageRoute(builder: (BuildContext context) => this.pushTo));
      },
    );
  }
}
