import 'package:flutter/material.dart';

import 'package:numa/king/king.dart';

class BasedCircleImage extends StatelessWidget {
  final String src;
  final String altText;
  final double radius;
  BasedCircleImage({String src, String altText, double radius = 80})
      : this.src = src,
        this.altText = altText,
        this.radius = radius,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return _buildImage(context);
  }

  Widget _buildImage(BuildContext context) {
    var king = fromKing(context, Viceroys.king);
    if (king.canLoadNetworkImage()) {
      return CircleAvatar(
          radius: this.radius,
          backgroundImage: NetworkImage(this.src),
          backgroundColor: Colors.orange,
          child: Text(this.altText, style: TextStyle(fontSize: 20)));
    }

    return CircleAvatar(
        radius: this.radius,
        backgroundColor: Colors.orange,
        child: Text(this.altText, style: TextStyle(fontSize: 20)));
  }
}
