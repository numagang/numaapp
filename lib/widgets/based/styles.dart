import 'package:flutter/material.dart';

optionInfoStyle() {
  return TextStyle(
    color: Colors.grey,
  );
}

failureStyle() {
  return TextStyle(
    color: Colors.red,
  );
}
