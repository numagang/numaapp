import 'package:flutter/material.dart';

import 'package:numa/king/king.dart';
import 'package:numa/king/lip.dart';
import 'package:numa/widgets/based/scaffold.dart';
import 'package:numa/widgets/based/styles.dart';

class ChangeUsername extends StatefulWidget {
  String userUuid;
  String initialValue;
  ChangeUsername({String this.userUuid, String this.initialValue});

  @override
  _ChangeUsernameState createState() => _ChangeUsernameState(
      userUuid: this.userUuid, initialValue: this.initialValue);
}

class _ChangeUsernameState extends State<ChangeUsername> {
  String userUuid;
  bool requestInProgress = false;
  String endpoint = 'account/change/username';
  String currentValue = '';

  _ChangeUsernameState({String this.userUuid, String initialValue})
      : this.currentValue = initialValue;

  @override
  Widget build(BuildContext context) {
    var account = getItemFromDad(context, CacheTypes.account, 'fakeUuid');
    var king = fromKing(context, Viceroys.king);

    return BasedScaffoldChangeForm(
      onConfirm: () {
        if (!this.requestInProgress) {
          this.requestInProgress = true;
          Future<String> failure = this._submitChange(king);
          // TODO: request
          this.requestInProgress = false;
          if (failure != null) {
            king.dad.accountCache.flagForUpdate(userUuid);
            Navigator.of(context).pop();
          } else {
            king.showSnackBar('Failed to submit change.');
          }
        }
      },
      child: SizedBox.expand(
        child: Container(
          color: Colors.grey[850],
          child: Padding(
            padding: EdgeInsets.all(24.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                TextFormField(
                    initialValue: this.currentValue,
                    enabled: !this.requestInProgress,
                    style: TextStyle(fontSize: 20, color: Colors.white),
                    onChanged: (newValue) {
                      this.currentValue = newValue;
                    }),
                SizedBox(height: 12),
                Text('Edit your username.', style: optionInfoStyle()),
                SizedBox(height: 12),
                Text('Username must be at least 4 characters.',
                    style: optionInfoStyle()),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Future<String> _submitChange(King king) async {
    String failure = null;
    String value = currentValue;

    var payload = {
      'userUuid': userUuid,
      'endpoint': this.endpoint,
      'value': value,
    };

    ApiResponse ares = await king.lip.api(endpoint, payload: payload);

    if (ares.isOk()) {
    } else {
      failure = 'Failed to change ${this.endpoint} to ${value}';
    }
    return failure;
  }
}
