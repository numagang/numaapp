import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';

import 'package:numa/dad/dad.dart';
import 'package:numa/widgets/account/change_username.dart';
import 'package:numa/widgets/based/images.dart';
import 'package:numa/widgets/based/scaffold.dart';

class AccountPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var account = getItemFromDad(context, CacheTypes.account, 'fakeUuid');

    return Container(
      color: Colors.grey[850],
      child: Observer(
        builder: (_) => Column(
          children: <Widget>[
            SizedBox(height: 20),
            BasedCircleImage(
                //src: this.profile.avatarUrl, altText: this.profile.initials),
                src: '',
                radius: 40,
                altText: account.username.substring(0, 2)),
            SizedBox(height: 20),
            OptionCluster(
              title: 'Account',
              children: <Widget>[
                OptionRow(
                    option: 'Username',
                    value: account.username,
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (BuildContext context) => ChangeUsername(
                                  userUuid: account.userUuid,
                                  initialValue: account.username)));
                    }),
                Divider(color: Colors.black, height: 0),
                OptionRow(
                    option: 'Bio',
                    value: account.bio,
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (BuildContext context) => ChangeBio()));
                    }),
              ],
            ),
            SizedBox(height: 20),
            OptionCluster(
              title: 'Settings',
              children: <Widget>[
                OptionRowIcon(
                    option: 'Notifications',
                    icon: Icons.notifications_none,
                    onTap: () {
                      //Navigator.push(
                      //context,
                      //MaterialPageRoute(
                      //builder: (BuildContext context) =>
                      //ChangeUsername()));
                    }),
                Divider(color: Colors.black, height: 0),
                OptionRowIcon(
                    option: 'Language',
                    icon: Icons.language,
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (BuildContext context) => ChangeBio()));
                    }),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

class OptionCluster extends StatelessWidget {
  String title;
  List<Widget> children;
  OptionCluster({this.title, this.children});

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.grey[800],
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Padding(
            padding: EdgeInsets.fromLTRB(16, 16, 16, 4),
            child: Text(this.title, style: TextStyle(color: Colors.green)),
          ),
          ...this.children,
        ],
      ),
    );
  }
}

class OptionRow extends StatelessWidget {
  String option;
  String value;
  Function onTap;
  OptionRow({this.option, this.value, this.onTap});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        this.onTap();
      },
      child: Padding(
        padding: EdgeInsets.fromLTRB(16, 14, 16, 14),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Text(this.value, style: TextStyle(color: Colors.grey[100])),
            SizedBox(height: 4),
            Text(this.option,
                style: TextStyle(color: Colors.grey, fontSize: 12)),
          ],
        ),
      ),
    );
  }
}

class OptionRowIcon extends StatelessWidget {
  String option;
  IconData icon;
  Function onTap;
  // TODO: semanticLabel
  OptionRowIcon({this.option, this.icon, this.onTap});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        this.onTap();
      },
      child: Padding(
        padding: EdgeInsets.fromLTRB(16, 14, 16, 14),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            SizedBox(height: 4),
            Row(
              children: <Widget>[
                Icon(icon, color: Colors.grey),
                SizedBox(width: 12),
                Text(this.option, style: TextStyle(color: Colors.grey[100])),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

class ChangeBio extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BasedScaffold(child: Text('Change bio'));
  }
}

class ChangeNotifications extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BasedScaffold(child: Text('Change notifications'));
  }
}

class ChangeLanguage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BasedScaffold(child: Text('Change language'));
  }
}
