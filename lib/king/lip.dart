import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:http/http.dart' as http;

import 'package:numa/king/king.dart';

class Lip extends http.BaseClient {
  String apiUrl;
  King king;
  final http.Client _inner = http.Client();
  String jwt;
  int timeout;

  Lip(this.king) {
    apiUrl = this.king.conf.apiUrl;
    timeout = this.king.conf.requestTimeout;
  }

  Future<ApiResponse> api(String endpoint,
      {Map<String, Object> payload}) async {
    Map<String, String> headers = {
      'content-type': 'application/json',
    };
    if (jwt != null) {
      headers['authorization'] = jwt;
    }

    String payloadAsString = jsonEncode(payload);
    king.log.fine(payloadAsString);

    // NOTE: if we are passed a full url, use that instead of papi
    String url;
    String protocol = endpoint.split(':').first;
    if (protocol == 'http' || protocol == 'https') {
      url = endpoint;
    } else {
      url = apiUrl + endpoint;
    }

    ApiResponse ares = ApiResponse();
    print('NAPI: sending request to ' + endpoint + ' with body: ');
    print(payloadAsString);

    try {
      http.Response response = await _inner
          .post(url,
              headers: headers,
              body: payloadAsString,
              encoding: Encoding.getByName('utf-8'))
          .timeout(Duration(seconds: timeout));
      ares.statusCode = response.statusCode;
      ares.body = json.decode(response.body);
      print('Response: ');
      print(ares.body);
    } on SocketException catch (e, s) {
      king.log.info('SocketException while connecting to API');
      ares.errored = true;
    } on TimeoutException catch (e, s) {
      king.log.info('TimeoutException while connecting to API');
      ares.errored = true;
    }

    return ares;
  }

  Future<http.StreamedResponse> send(http.BaseRequest request) {
    // TODO: is this ever reached?
    return _inner.send(request);
  }
}

class ApiResponse {
  int statusCode = 0;
  Map<String, Object> body = {};
  bool errored = false;

  isOk() {
    if (this.errored) {
      return false;
    }
    if (this.body == null) {
      return false;
    }
    return (statusCode >= 200) && (statusCode <= 299);
  }

  isOkWithoutBody() {
    if (this.errored) {
      return false;
    }
    return statusCode == HttpStatus.ok;
  }
}
