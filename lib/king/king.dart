import 'package:flutter/material.dart';
import 'package:logging/logging.dart';
import 'package:mobx/mobx.dart';
import 'package:provider/provider.dart';

import 'package:numa/conf.dart';
import 'package:numa/dad/dad.dart';
import 'package:numa/king/lip.dart';
import 'package:numa/king/mock/lip.dart';

part 'king.g.dart';

class King {
  BuildContext context;
  Conf conf;
  Dad dad;
  Logger log;
  Lip lip;
  NavManager navman;
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();

  King({Conf conf}) {
    // no dependencies
    this.conf = conf;
    this.navman = new NavManager();

    // depends only on conf
    this.log = new Logger('NumaKing');
    setupLogger(conf.logLevel);

    // depends on 'this'
    //this.user = new User(this);
    this.dad = new Dad(this);

    switch (conf.whichApi) {
      case ApiChoices.napi:
        this.lip = new Lip(this);
        break;
      case ApiChoices.mock:
        this.lip = new MockLip(this);
        break;
      default:
        conf.printApiChoice();
        throw ('Invalid api set by conf.whichApi');
        break;
    }
  }

  void showSnackBar(String text) {
    // can use this to show notification after navigation
    final snackBar = SnackBar(content: Text(text));
    scaffoldKey.currentState.showSnackBar(snackBar);
  }

  void showSnackBarRequestFailed() {
    final snackBar = SnackBar(content: Text('Request failed.'));
    scaffoldKey.currentState.showSnackBar(snackBar);
  }

  void setContext(BuildContext context) {
    this.context = context;
  }

  bool canLoadNetworkImage() {
    switch (this.conf.whichApi) {
      case ApiChoices.napi:
        return true;
      case ApiChoices.mock:
        return false;
      default:
        this.conf.printApiChoice();
        throw ('Invalid api set by conf.whichApi');
        break;
    }
  }
}

enum NavIndices { account, roll, convo, auth }

class NavManager = NavManagerBase with _$NavManager;

abstract class NavManagerBase with Store {
  @observable
  var currentIndex = NavIndices.convo;

  final accountNavKey = GlobalKey<NavigatorState>();
  final rollNavKey = GlobalKey<NavigatorState>();
  final convoNavKey = GlobalKey<NavigatorState>();
  final authNavKey = GlobalKey<NavigatorState>();

  //@computed?
  isCurrentIndex(NavIndices index) {
    if (this.currentIndex == index) {
      return true;
    }
    return false;
  }

  @action
  void switchToNav(NavIndices newIndex) {
    currentIndex = newIndex;
    print('switchToNav newIdex ${newIndex.index}');
    //switch (currentIndex) {
    //case NavIndices.account:
    //accountNavKey.currentState.popUntil((route) => route.isFirst);
    //break;
    //case NavIndices.roll:
    //rollNavKey.currentState.popUntil((route) => route.isFirst);
    //break;
    //case NavIndices.convo:
    //convoNavKey.currentState.popUntil((route) => route.isFirst);
    //break;
    //case NavIndices.auth:
    //authNavKey.currentState.popUntil((route) => route.isFirst);
    //break;
    //default:
    //}
  }
}

void setupLogger(String level) {
  switch (level) {
    case 'off': // 2000
      Logger.root.level = Level.OFF;
      break;
    case 'shout': // 1200
      Logger.root.level = Level.SHOUT;
      break;
    case 'severe': // 1000
      Logger.root.level = Level.SEVERE;
      break;
    case 'warning': // 900
      Logger.root.level = Level.WARNING;
      break;
    case 'info': // 800
      Logger.root.level = Level.INFO;
      break;
    case 'config': // 700
      Logger.root.level = Level.CONFIG;
      break;
    case 'fine': // 500
      Logger.root.level = Level.FINE;
      break;
    case 'finer': // 400
      Logger.root.level = Level.FINER;
      break;
    case 'finest': // 300
      Logger.root.level = Level.FINEST;
      break;
    case 'all': // 0
      Logger.root.level = Level.ALL;
      break;
  }

  Logger.root.onRecord.listen((record) {
    print('${record.level.name}: ${record.time}: ${record.message}');
  });
}

enum Viceroys { conf, dad, king, lip, log, navman, user, userLoc }

fromKing(BuildContext context, Viceroys viceroy) {
  final King king = Provider.of<King>(context, listen: false);
  switch (viceroy) {
    case (Viceroys.conf):
      return king.conf;
    //case (Viceroys.dad):
    //return king.dad;
    case (Viceroys.king):
      return king;
    //case (Viceroys.lip):
    //return king.lip;
    //case (Viceroys.log):
    //return king.log;
    //case (Viceroys.navman):
    //return king.navman;

    //case (Viceroys.user):
    //return king.user;
    //case (Viceroys.userLoc):
    //return king.user.userLoc;
  }
}
