import 'dart:io';

import 'package:numa/king/lip.dart';

ApiResponse mockAccount(String endpoint, {Map<String, Object> payload}) {
  ApiResponse ares = ApiResponse();

  switch (endpoint) {
    case 'account/get':
      ares.statusCode = HttpStatus.ok;
      ares.body = {
        'username': 'icup69',
        'userUuid': '1111',
        'bio': 'i just want to grill',
      };
      break;

    case 'account/change/username':
      ares.statusCode = HttpStatus.accepted; // 202
      break;

    default:
      throw ('ERROR: url not found in mock');
      break;
  }
  return ares;
}
