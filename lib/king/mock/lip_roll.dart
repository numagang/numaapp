import 'dart:io';

import 'package:numa/king/lip.dart';

ApiResponse mockRoll(String endpoint, {Map<String, Object> payload}) {
  ApiResponse ares = ApiResponse();

  switch (endpoint) {
    case 'roll/memeData':
      ares.statusCode = HttpStatus.ok;
      ares.body = {
        'reactedMemesInfo': {},
        'allMemesSet': [
          '012340',
          '012341',
          '012342',
          '012343',
          '012344',
          '012345',
          '012346',
          '012347',
          '012348',
          '012349',
          '012350',
          '012351',
          '012352',
          '012353',
          '012354',
          '012355',
          '012356',
          '012357',
          '012358',
          '012359',
          '012360',
          '012361',
          '012362',
          '012363',
          '012364',
          '012365',
          '012366',
        ],
        'unreactedMemesSet': [
          '012340',
          '012341',
          '012342',
          '012343',
          '012344',
          '012345',
          '012346',
          '012347',
          '012348',
          '012349',
          '012350',
          '012351',
          '012352',
          '012353',
          '012354',
          '012355',
          '012356',
          '012357',
          '012358',
          '012359',
          '012360',
          '012361',
          '012362',
          '012363',
          '012364',
          '012365',
          '012366',
        ],
      };
      break;

    case 'roll/reaction.submit':
      // TODO: if payload userUuid...?
      ares.statusCode = HttpStatus.ok;
      // TODO: test ignored
      ares.body = {
        'action': 'reacted',
        'memeUuid': payload['memeUuid'],
        'reactionName': payload['reactionName'],
      };
      break;

    default:
      throw ('ERROR: url not found in mock');
      break;
  }
  return ares;
}
