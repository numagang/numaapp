import 'dart:io';

import 'package:numa/dad/convo_cache.dart';
import 'package:numa/king/lip.dart';

ApiResponse mockConvo(String endpoint, {Map<String, Object> payload}) {
  ApiResponse ares = ApiResponse();

  switch (endpoint) {
    case 'convo/get':
      ares.statusCode = HttpStatus.ok;
      ares.body = generateConvoData();
      break;

    default:
      throw ('ERROR: url not found in mock');
      break;
  }
  return ares;
}

Map<String, dynamic> generateConvoData() {
  Map<String, dynamic> body = {
    'messageList': [
      {
        'msgUuid': '10',
        'userUuid': '2222',
        'username': 'Danny',
        'text': 'hi neighbor',
        'time': DateTime.now().millisecondsSinceEpoch,
      },
      {
        'msgUuid': '11',
        'userUuid': '1111',
        'username': 'Tommy',
        'text': 'hey there what is up',
        'time': DateTime.now().millisecondsSinceEpoch,
      },
      {
        'msgUuid': '12',
        'userUuid': '1111',
        'username': 'Tommy',
        'text': 'hey there what is up',
        'time': DateTime.now().millisecondsSinceEpoch,
      },
      {
        'msgUuid': '13',
        'userUuid': '1111',
        'username': 'Tommy',
        'text': 'woops sorry I sent that last message TWICE',
        'time': DateTime.now().millisecondsSinceEpoch,
      },
      {
        'msgUuid': '14',
        'userUuid': '1111',
        'username': 'Tommy',
        'text': 'SRY CAPS',
        'time': DateTime.now().millisecondsSinceEpoch,
      },
      {
        'msgUuid': '15',
        'userUuid': '3333',
        'username': 'Becky',
        'text': 'great memes',
        'time': DateTime.now().millisecondsSinceEpoch,
      },
      {
        'msgUuid': '16',
        'userUuid': '3333',
        'username': 'Becky',
        'text':
            'great times great times great times great times great times great times great times great times - seaaaa lab, underneath the water, seaaa lab, at the bottom of the sea',
        'time': DateTime.now().millisecondsSinceEpoch,
      },
      {
        'msgUuid': '17',
        'userUuid': '1111',
        'username': 'Tommy',
        'text':
            'Hey i can do that too great times great times great times great times great times great times great times great times - seaaaa lab, underneath the water, seaaa lab, at the bottom of the sea',
        'time': DateTime.now().millisecondsSinceEpoch,
      },
      {
        'msgUuid': '18',
        'userUuid': '3333',
        'username': 'Becky',
        'text': 'cutt it out',
        'time': DateTime.now().millisecondsSinceEpoch,
      },
      {
        'msgUuid': '19',
        'userUuid': '3333',
        'username': 'Becky',
        'text': 'cutt it out',
        'time': DateTime.now().millisecondsSinceEpoch,
      },
    ],
  };

  return body;
}
