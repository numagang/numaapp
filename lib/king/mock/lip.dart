import 'dart:async';
import 'dart:io';
import 'dart:math';

import 'package:numa/king/king.dart';
import 'package:numa/king/lip.dart';
import 'package:numa/king/mock/lip_account.dart';
import 'package:numa/king/mock/lip_convo.dart';
import 'package:numa/king/mock/lip_convo_list.dart';
import 'package:numa/king/mock/lip_roll.dart';

class MockLip extends Lip {
  King king;
  MockLip(this.king) : super(king);

  @override
  Future<ApiResponse> api(String endpoint,
      {Map<String, Object> payload}) async {
    ApiResponse ares = ApiResponse();

    var rng = new Random();
    int randomWait = rng.nextInt(500);
    sleep(Duration(milliseconds: randomWait));

    //await new Future.delayed(const Duration(seconds: 2));

    print('MOCK API: attempting to reach ' + endpoint + ' with payload: ');
    print(payload);

    List<String> endpointPieces = endpoint.split('/');

    switch (endpointPieces.first) {
      case 'roll':
        ares = mockRoll(endpoint, payload: payload);
        break;

      case 'account':
        ares = mockAccount(endpoint, payload: payload);
        break;

      case 'convo':
        ares = mockConvo(endpoint, payload: payload);
        break;

      case 'convoList':
        ares = mockConvoList(endpoint, payload: payload);
        break;

      default:
        throw ('ERROR: url not found in mock');
    }

    return ares;
  }
}
