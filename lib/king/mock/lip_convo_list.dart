import 'dart:io';

import 'package:numa/dad/convo_list_cache.dart';
import 'package:numa/king/lip.dart';

ApiResponse mockConvoList(String endpoint, {Map<String, Object> payload}) {
  ApiResponse ares = ApiResponse();

  switch (endpoint) {
    case 'convoList/get':
      ares.statusCode = HttpStatus.ok;
      ares.body = generateConvoListData();
      break;

    default:
      throw ('ERROR: url not found in mock');
      break;
  }
  return ares;
}

Map<String, dynamic> generateConvoListData() {
  Map<String, dynamic> body = {
    'convoList': [
      {
        'convoUuid': '1111',
        'title': 'Knuckles gang',
        'lastMessage':
            'I really like you, guy. You might be the best friend I have ever had',
        'lastMessageUsername': 'Todd',
        'lastMessageTime': DateTime.now().millisecondsSinceEpoch,
        'unreadCount': 64,
        'atCount': 2,
        'participants': ['1111', '2222', '3333'],
      },
      {
        'convoUuid': '1111',
        'title': 'Tails Gang',
        'lastMessage': 'tails my guy',
        'lastMessageUsername': 'ilovetails',
        'lastMessageTime': DateTime.now().millisecondsSinceEpoch,
        'unreadCount': 64,
        'atCount': 2,
        'participants': ['1111', '2222', '3333'],
      },
    ],
  };

  return body;
}
