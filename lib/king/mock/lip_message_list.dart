import 'dart:io';

import 'package:numa/dad/convo_cache.dart';
import 'package:numa/king/lip.dart';

ApiResponse mockConvo(String endpoint, {Map<String, Object> payload}) {
  ApiResponse ares = ApiResponse();

  switch (endpoint) {
    case 'convo/get':
      ares.statusCode = HttpStatus.ok;
      ares.body = generateConvoData();
      break;

    default:
      throw ('ERROR: url not found in mock');
      break;
  }
  return ares;
}

Map<String, dynamic> generateConvoData() {
  Map<String, dynamic> body = {
    'convo': [
      {
        'messageUuid': '1111',
        'title': 'Knuckles gang',
        'lastMessage':
            'I really like you, guy. You might be the best friend I have ever had',
        'lastMessageUsername': 'Todd',
        'lastMessageTime': DateTime.now().millisecondsSinceEpoch,
        'unreadCount': 64,
        'atCount': 2,
        'participants': ['1111', '2222', '3333'],
      },
      {
        'messageUuid': '1111',
        'title': 'Tails Gang',
        'lastMessage': 'tails my guy',
        'lastMessageUsername': 'ilovetails',
        'lastMessageTime': DateTime.now().millisecondsSinceEpoch,
        'unreadCount': 64,
        'atCount': 2,
        'participants': ['1111', '2222', '3333'],
      },
    ],
  };
  return body;
}
