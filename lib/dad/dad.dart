import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:numa/king/king.dart';
import 'package:numa/dad/account_cache.dart';
import 'package:numa/dad/convo_cache.dart';
import 'package:numa/dad/convo_list_cache.dart';
import 'package:numa/dad/roll_manager_cache.dart';

// data acquisition director (ngl i forgot the original acronym)
// data advisor might be better :thinking:
class Dad {
  King king;

  AccountCache accountCache;
  ConvoCache convoCache;
  ConvoListCache convoListCache;
  RollManagerCache rollManagerCache;

  Dad(this.king) {
    this.accountCache = new AccountCache(this.king);
    this.convoCache = new ConvoCache(this.king);
    this.convoListCache = new ConvoListCache(this.king);
    this.rollManagerCache = new RollManagerCache(this.king);
  }
}

enum CacheTypes { account, convo, convoList, rollManager }

fromDad(BuildContext context, CacheTypes cacheType) {
  final King king = Provider.of<King>(context, listen: false);
  switch (cacheType) {
    case (CacheTypes.account):
      return king.dad.accountCache;
    case (CacheTypes.convo):
      return king.dad.convoCache;
    case (CacheTypes.convoList):
      return king.dad.convoListCache;
    case (CacheTypes.rollManager):
      return king.dad.rollManagerCache;
  }
}

getItemFromDad(BuildContext context, CacheTypes cacheType, String uuid) {
  final King king = Provider.of<King>(context, listen: false);
  var cache = fromDad(context, cacheType);
  return cache.getItem(uuid);
}
