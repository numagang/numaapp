import 'package:mobx/mobx.dart';

import 'package:numa/dad/cache.dart';
import 'package:numa/king/king.dart';
part 'convo_list_cache.g.dart';

class ConvoListCache extends Cache<ConvoList> {
  King king;
  String getUrl = 'convoList/get';

  ConvoListCache(this.king) : super(king);

  @override
  ConvoList makeNewCacheItem() {
    return ConvoList();
  }

  @action
  unpackItemFromApi(Map<String, Object> jsonIn, ConvoList target) {
    target.unpackConvoList(jsonIn['convoList']);
  }
}

class ConvoList = ConvoListBase with _$ConvoList;

abstract class ConvoListBase with Store {
  @observable
  var convoList = ObservableList<ConvoInfo>();

  @action
  void addConvoInfo(ConvoInfo convoInfo) => this.convoList.add(convoInfo);

  @action
  void removeConvoInfo(ConvoInfo convoInfo) => this.convoList.remove(convoInfo);

  @action
  void clear() => convoList.clear();

  unpackConvoList(List<dynamic> dynIn) {
    this.convoList.clear();
    for (var item in dynIn) {
      var convoInfo = ConvoInfo.fromJson(item);
      this.addConvoInfo(convoInfo);
    }
  }
}

class ConvoInfo {
  String convoUuid;
  String title;
  String lastMessage;
  String lastMessageUsername;
  int lastMessageTime;
  int expirationTime;
  int unreadCount;
  int atCount;
  List<String> participants;

  ConvoInfo({
    this.convoUuid,
    this.title,
    this.lastMessage,
    this.lastMessageUsername,
    this.lastMessageTime,
    this.expirationTime,
    this.unreadCount,
    this.atCount,
    this.participants,
  });

  factory ConvoInfo.fromJson(Map<String, dynamic> jsonIn) => ConvoInfo(
        convoUuid: jsonIn['convoUuid'],
        title: jsonIn['title'],
        lastMessage: jsonIn['lastMessage'],
        lastMessageUsername: jsonIn['lastMessageUsername'],
        lastMessageTime: jsonIn['lastMessageTime'],
        expirationTime: jsonIn['expirationTime'],
        unreadCount: jsonIn['unreadCount'],
        atCount: jsonIn['atCount'],
        participants: jsonIn['participants'],
      );

  Map<String, dynamic> toJson() => {
        'convoUuid': convoUuid,
        'title': title,
        'lastMessage': lastMessage,
        'lastMessageUsername': lastMessageUsername,
        'lastMessageTime': lastMessageTime,
        'expirationTime': expirationTime,
        'unreadCount': unreadCount,
        'atCount': atCount,
        'participants': participants,
      };

  String expirationTimeReadable() {
    return 'Tuesday';
  }
}
