import 'package:mobx/mobx.dart';

import 'package:numa/dad/cache.dart';
import 'package:numa/king/king.dart';
part 'account_cache.g.dart';

class AccountCache extends Cache<Account> {
  King king;
  String getUrl = 'account/get';
  String updateFieldUrl = 'account/field.update';

  AccountCache(this.king) : super(king);

  @override
  Account makeNewCacheItem() {
    return Account();
  }

  @action
  unpackItemFromApi(Map<String, Object> jsonIn, Account target) {
    target.username = jsonIn['username'];
    target.userUuid = jsonIn['userUuid'];
    target.bio = jsonIn['bio'];
  }
}

class Account = AccountBase with _$Account;

abstract class AccountBase with Store {
  @observable
  String username = 'Loading...';

  @observable
  String bio = 'Loading...';

  @observable
  String userUuid = 'Loading...';
}
