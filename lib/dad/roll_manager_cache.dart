import 'dart:async';
import 'dart:math';
import 'package:mobx/mobx.dart';

import 'package:numa/dad/cache.dart';
import 'package:numa/king/king.dart';
import 'package:numa/king/lip.dart';
part 'roll_manager_cache.g.dart';

class RollManagerCache extends Cache<RollManager> {
  King king;
  String getUrl = 'roll/memeData';

  RollManagerCache(this.king) : super(king);

  @override
  RollManager makeNewCacheItem() {
    return RollManager(this.king);
  }

  @action
  unpackItemFromApi(Map<String, Object> jsonIn, RollManager target) {
    target.allMemesSet = Set.from(jsonIn['allMemesSet']);
    target.unreactedMemesSet = Set.from(jsonIn['unreactedMemesSet']);
    // TODO: unpack reactedMemesInfo
    //target.reactedMemesInfo = jsonIn['reactedMemesInfo'];
    target.updateCurrentMeme();
  }
}

class RollManager = RollManagerBase with _$RollManager;

abstract class RollManagerBase with Store {
  King king;
  bool submitReactionInProgress = false;
  String submitReactionUrl = 'roll/reaction.submit';
  // TODO: keep on disk
  Map<String, Reactions> reactedMemesInfo = new Map();
  Set<String> allMemesSet = new Set();
  Set<String> unreactedMemesSet = new Set();

  @observable
  bool loadingMemeData = true;

  @observable
  bool noMoreMemes = false;

  @observable
  String currentMemeUuid = ''; // TODO: loading image

  @action
  updateCurrentMeme() {
    if (this.unreactedMemesSet.length == 0) {
      if (this.reactedMemesInfo.length != 0) {
        this.noMoreMemes = true;
        return;
      }
      // NOTE: in this case we are downloading memeData
      this.loadingMemeData = true;
      return;
    }

    if (this.unreactedMemesSet.contains(this.currentMemeUuid)) {
      // NOTE: in this case we keep displaying the current meme
      return;
    }

    var rng = new Random();
    int rand = rng.nextInt(this.unreactedMemesSet.length);
    this.currentMemeUuid = this.unreactedMemesSet.elementAt(rand);
    this.loadingMemeData = false;
  }

  RollManagerBase(this.king) {
    // TODO: load reactedMemes from disk
  }

  @action
  Future<int> submitReaction(Reactions reaction, String userUuid) async {
    String reactionName = reaction.toString().split('.').last;
    if (this.submitReactionInProgress == false) {
      this.submitReactionInProgress = true;
      var payload = {
        'userUuid': userUuid,
        'memeUuid': this.currentMemeUuid,
        'reactionName': reactionName,
      };
      ApiResponse ares =
          await this.king.lip.api(submitReactionUrl, payload: payload);
      this.submitReactionInProgress = false;

      if (ares.isOk()) {
        this.updateReactionLists(ares.body);
        return 0;
      } else {
        this.king.showSnackBarRequestFailed();
        return 1;
      }
    }
  }

  updateReactionLists(Map<String, Object> body) {
    if (body['action'] == 'reacted') {
      this.reactedMemesInfo[body['memeUuid']] = Reactions.values.firstWhere(
          (e) => e.toString() == 'Reactions.' + body['reactionName']);
      this.unreactedMemesSet.remove(body['memeUuid']);
    } else if (body['action'] == 'alreadyReacted') {
      this.unreactedMemesSet.remove(body['memeUuid']);
    } else {
      // TODO: report error
    }

    print(this.unreactedMemesSet);

    this.updateCurrentMeme();
  }
}

//enum RollState {
//bootingUp,
//noMoreMemes,
//}

enum Reactions {
  angryRed,
  cry,
  laughTears,
  heartEyes,
}
