import 'dart:async';

import 'package:numa/king/king.dart';
import 'package:numa/king/lip.dart';

// TODO: if multiple requests for the same item are sent at once, it might be
// good to have logic such that the latest version of the request wins. This
// is probably best achieved by appending a version number to every object. Or,
// this error could be ignored.

abstract class Cache<T> {
  // TODO: ability to cancel requests?
  // TODO: implement cachedTimes
  Map<String, T> cachedItems = {};
  Map<String, int> cachedTimes = {};
  Set<String> flaggedForUpdate = {};
  Set<String> requestInProgress = {};
  String getUrl;
  String updateFieldUrl;
  King king;

  Cache(this.king);

  T makeNewCacheItem() {
    throw UnimplementedError;
  }

  void unpackItemFromApi(Map<String, Object> jsonIn, T target) {
    throw UnimplementedError;
  }

  void getItemFromApi(String uuid, {Map<String, Object> payload}) async {
    if (requestInProgress.contains(uuid)) {
      return;
    }

    var target = this.cachedItems[uuid];
    if (payload == null) {
      payload = {
        'uuid': uuid,
      };
    }

    requestInProgress.add(uuid);
    ApiResponse ares = await this.king.lip.api(this.getUrl, payload: payload);
    requestInProgress.remove(uuid);

    if (ares.errored) {
      this.flaggedForUpdate.add(uuid);
    }

    if (ares.isOk()) {
      this.unpackItemFromApi(ares.body, target);
    } else {
      this.king.showSnackBarRequestFailed();
    }
  }

  void putItem(String uuid, T item) {
    print("WARNING: this function is only for testing");
    cachedItems[uuid] = item;
  }

  T getItem(String uuid, {Map<String, Object> payload}) {
    if (this.cachedItems.containsKey(uuid)) {
      if (this.shouldUpdate(uuid)) {
        this.getItemFromApi(uuid, payload: payload);
      }
    } else {
      this.cachedItems[uuid] = makeNewCacheItem();
      this.getItemFromApi(uuid, payload: payload);
    }
    return this.cachedItems[uuid];
  }

  bool shouldUpdate(String uuid) {
    if (!cachedItems.containsKey(uuid)) {
      return true;
    }
    if (flaggedForUpdate.contains(uuid)) {
      return true;
    }
    int msNow = DateTime.now().toUtc().millisecondsSinceEpoch;
    if (msNow > msNow + this.king.conf.cacheTimeoutMinutes * 60000) {
      return true;
    }
    return false;
  }

  flagForUpdate(String uuid) {
    flaggedForUpdate.add(uuid);
  }

  void updateField(String uuid, String fieldName, String fieldValue) async {
    var target = this.cachedItems[uuid];
    Map<String, Object> payload = {
      'uuid': uuid,
      'fieldName': fieldName,
      'fieldValue': fieldValue,
    };
    ApiResponse ares = await this.king.lip.api(this.getUrl, payload: payload);
    if (ares.isOk()) {
      this.unpackItemFromApi(ares.body, target);
    } else {
      this.king.showSnackBarRequestFailed();
    }
  }
}
