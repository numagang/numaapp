import 'package:mobx/mobx.dart';

import 'package:numa/dad/cache.dart';
import 'package:numa/king/king.dart';
part 'convo_cache.g.dart';

class ConvoCache extends Cache<Convo> {
  King king;
  String getUrl = 'convo/get';

  ConvoCache(this.king) : super(king);

  @override
  Convo makeNewCacheItem() {
    return Convo();
  }

  @action
  unpackItemFromApi(Map<String, Object> jsonIn, Convo target) {
    target.unpackConvo(jsonIn['messageList']);
  }
}

class Convo = ConvoBase with _$Convo;

// NOTE: a convo is a message list - saying convo just feels very natural
abstract class ConvoBase with Store {
  @observable
  var messageList = ObservableList<Message>();

  @action
  void addMessage(Message msg) => this.messageList.add(msg);

  @action
  void removeMessage(Message msg) => this.messageList.remove(msg);

  @action
  void clear() => messageList.clear();

  unpackConvo(List<dynamic> dynIn) {
    this.messageList.clear();
    for (var item in dynIn) {
      var msg = Message.fromJson(item);
      this.addMessage(msg);
    }
  }
}

class Message {
  String msgUuid;
  String userUuid;
  // TODO: store usernames somepleace else to reduce this data?
  String username;
  String text;
  // TODO: images
  int time;
  // TODO: sort/filter tags
  //List<String> tags;
  // TODO: sort/filter ats. highlight bubbles with a user's at?
  //List<String> ats;
  //bool seen;
  // TODO: reply & quote
  // TODO: forward
  // TODO: edited

  String timeReadable() {
    return '4:21 pm';
  }

  Message({
    this.msgUuid,
    this.userUuid,
    this.username,
    this.text,
    this.time,
  });

  factory Message.fromJson(Map<String, dynamic> jsonIn) => Message(
        msgUuid: jsonIn['msgUuid'],
        userUuid: jsonIn['userUuid'],
        username: jsonIn['username'],
        text: jsonIn['text'],
        time: jsonIn['time'],
      );

  Map<String, dynamic> toJson() => {
        'msgUuid': msgUuid,
        'userUuid': userUuid,
        'username': username,
        'text': text,
        'time': time,
      };

  String expirationTimeReadable() {
    return 'Tuesday';
  }
}
