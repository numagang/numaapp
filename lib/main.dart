import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:provider/provider.dart';

import 'package:numa/conf.dart';
import 'package:numa/l10n/localization.dart';
import 'package:numa/king/king.dart';

import 'package:numa/nav_stack.dart';

void main() async {
  // NOTE: you can select real api or use mock here for testing
  Conf conf = Conf(whichApi: ApiChoices.mock);
  //Conf conf = Conf(whichApi: ApiChoices.papi);
  King king = King(conf: conf);
  runApp(NumaApp(king: king));
}

class NumaApp extends StatelessWidget {
  final King king;
  NumaApp({this.king}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    this.king.setContext(context);

    return MaterialApp(
      home: Provider(
        create: (_) => this.king,
        lazy: false,
        child: NavStack(),
      ),
      theme: ThemeData(
        primarySwatch: Colors.green,
        // TODO:
        // visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      localizationsDelegates: [
        AppLocalizationsDelegate(),
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
      ],
      supportedLocales: [
        Locale('en', ''),
        Locale('ru', ''),
      ],
      onGenerateTitle: (BuildContext context) =>
          AppLocalizations.of(context).title,
    );
  }
}
